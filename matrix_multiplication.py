# matrix mul with shape [2,2]
y = [[1,2],[3,4]]
z = [[5,6],[7,8]]

x = [[0,0],[0,0]]
sum = 0
for i in range(len(y)):
    for j in range(len(y[0])):
        print(i, j)
        x[i][j] = y[i][j] * z[i][j]
        sum = sum+x[i][j]
print(x) # [[5, 12], [21, 32]]
print(sum)


# matrix mul with shape [2,3]
y = [[1,2,3],[4,5,6]]
z = [[7,8,9],[10,11,12]]
x = [[0,0,0],[0,0,0]]
sum = 0
for i in range(len(y)):
    for j in range(len(y[0])):
        print(i, j)
        x[i][j] = y[i][j] * z[i][j]
        sum = sum+x[i][j]
print(x) # [[7, 16, 27], [40, 55, 72]]
print(sum)

