import math
import numpy as np
y_actual = [1,2,3,4,5]
y_predicted = [1.6,2.5,2.9,3,4.1]
 
difference = np.subtract(y_actual,y_predicted)
print("Difference between individual datapoints:")
print(difference)
MSE = np.square(difference).mean() 
print("MSE:")
print(MSE)
 
RMSE = math.sqrt(MSE)
print("Root Mean Square Error:")
print(RMSE)
